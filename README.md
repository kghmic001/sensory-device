# Sensory Device 
This is the repo containing the information for our EEE3088 HAT project.

# Collaborators
Michael Keogh

John Michael

Nicola Watt

# Repository Structure
Design Proposal:
    
    Contains the BOM
    Contains Assignment 2

PCB Design Template:
    
    Contains Assigment 3
    Contains sub-module KiCad .zip folders

