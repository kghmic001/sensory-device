The HAT is a sensory PCB design located on GitLab. In the GIT repository, the GitLab development kit allows for code to be written and tested, from which contributions to the project can be made. 
The contribution doc is a separate file in the repo. 
All contributions to the pressure and temperature HAT (Hardware Attached on Top) are welcomed. The following ‘How-to-Guide’ will provide instructions on how to contribute to the project and the preferred methods for different scenarios. 

Step 1: Initial Conditions
Before contributing to the project, please consider the following: 
a)	External contributors are encouraged. Community involvement is valued but note potential restrictions in place.
b)	A separate CONTRIBUTING.md file is available for project-specific guidelines. This must be referenced.
Step 2: Reporting Issues:
If a bug, error, or potential risk is encountered, feedback on these problems is appreciated. To report these, follow the steps below: 
a)	Issue Tracker: check if GitLab’s issue tracker was used for the project. Issues can be raised on this platform.
b)	An issue template is provided. Follow the structure and provide the requested information, clearly and concisely. The general guide to the process is:
-	Test the problem in the context of the whole project. 
-	Liaise with a 3rd party to ensure the problem is relevant and needs attention.
-	Submit problem. 

Step 3: Contributing
To contribute, enhance or extend our code, design, and repository, follow the sub-steps below:
a)	Fork the repository: Creates a copy of the repository in your account.
b)	Make the changes to the various elements. Save the changes.
c)	Test the changes against pre-set specifications and requirements. 
d)	Commit and Push changes to fork repository. 
e)	Pull Request: Create a pull request in the original project. A banner is visible indicating that you pushed a new branch. Click on “Compare and pull request.” Explain changes made in request details block. 
f)	Review: The request will be reviewed, and feedback will be provided by the design team. 

Step 4: Merging
A merge request will be created, and the “Community contribution” label will be applied. If the contribution review processed is completed, (merge request coach), the changes will be merged into the repository. If a merge request was not automatically assigned, ask for a review by typing @gitlab-botready in a comment. 

Step 5: Tools Required
1)	Git: GitLab is accessed online, and an account is needed. 
2)	GitBash: A command-line platform to add changes and make commits.
3)	Cube IDE: Used for writing, testing and debugging the code for the PCB. 
4)	Text Editor: Notepad++ can suffice for making code modifications. 
5)	KiCad: Design software needed to view desing
6)	Collaboration and Communication Tools: Discord is an effective tool for communicating with a larger audience. It enables discussions, tracking and co-ordination with other contributors. 

NB: To ask for help with any step, type @gitlab-bot help in the comments. 

